Object.keys(page).forEach(key => {
  // loop through all devices of each page
  let environment = page[key]
  let dName = filenamifyExtra(key)
  let compareImages = []

  function doneReading() {
    if (++filesRead < 2) return
    resemble(img1)
      .compareTo(img2)
      .onComplete(function(data) {
        let mismatchTreshold = 0.1
        if (Number(data.misMatchPercentage) >= 0.1) {
          console.log(
            imgName,
            'mismatch percentage greater than ' + mismatchTreshold
          )
        }
        data
          .getDiffImage()
          .pack()
          .pipe(
            fs.createWriteStream(resultDirectory + '/diff-' + imgName + '.png')
          )
        console.log(imgName, 'Diff image created')
      })
  }

  Object.keys(environment).forEach(key => {
    // loop though all environments of each device of each page
    let image = environment[key]
    compareImages.push(image)
  })

  console.log(
    'Comparing',
    checkedUrl == '' ? 'homepage' : checkedUrl,
    'snapshots'
  )

  let imgName =
    checkedUrl == '' ? 'homepage-' + dName : checkedUrl + '-' + dName

  let filesRead = 0

  fs.readFile(compareImages[0], (err, data) => {
    img1 = data
    doneReading()
  })
  fs.readFile(compareImages[1], (err, data) => {
    img2 = data
    doneReading()
  })
})
