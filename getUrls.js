const request = require('request-promise')
const parse = require('xml-parser')

const instances = require('./configuration.js')

async function getUrls() {
  let baseUrl = instances[0].base
  let urls = parse(await request(baseUrl + '/sitemap.xml')).root.children.map(
    item => {
      return item.children
        .find(item => {
          return item.name === 'loc'
        })
        .content.replace(baseUrl, '')
    }
  )
  console.log('Number of URLs to test: ' + urls.length)
  return urls
}

module.exports = getUrls
