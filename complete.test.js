const assert = require('assert')
// const should = require('should')

describe('Page containg the video player', function () {
  it('should be paused on load', function () {
    var player = $('.carodaVideo')
    player.waitForVisible(3000)
    assert.notEqual($('.carodaVideo .video-js').getAttribute('class').indexOf('vjs-paused'), -1)
  })

  it('should have visible play button in the center on load', function () {
    var playButtonBig = $('.carodaVideo .video-js .vjs-big-play-button')
    playButtonBig.waitForVisible(3000)
    assert.equal(playButtonBig.isVisible(), true)
    assert.notEqual(playButtonBig.getCssProperty('display').value, 'none')
    assert.notEqual(playButtonBig.getCssProperty('visibility').value, 'hidden')
  })

  it('should display video player correctly with viewport width 992px', function () {
    if (browser.isMobile) {
      this.skip()
    } else {
      browser.setViewportSize({ width: 992, height: 600 })
      // browser.getViewportSize('width').should.be.equal(992)
    }
  })

  it('should display video player correctly with viewport width 768px', function () {
    if (browser.isMobile) {
      this.skip()
    } else {
      browser.setViewportSize({ width: 768, height: 400 })
      // browser.getViewportSize('width').should.be.equal(768)
    }
  })

  it('should hide play button in the center of the player on click', function () {
    if (browser.desiredCapabilities.browserName === 'iPhone') {
      this.skip()
    } else {
      var player = $('.carodaVideo')
      var playButtonBig = $('.carodaVideo .video-js .vjs-big-play-button')
      assert.equal(player.getAttribute('class').indexOf('vjs-waiting'), -1)
      assert.equal(player.getAttribute('class').indexOf('vjs-has-started'), -1)
      try {
        playButtonBig.click()
      } catch (err) {
        player.click()
      }
      browser.waitUntil(function () {
        return !playButtonBig.isVisible()
      }, 3000, 'expected loading button to hide when clicked')
      assert.equal(playButtonBig.getCssProperty('display').value, 'none')
    }
  })

  it('should hide loading spinner when video starts playing', function () {
    if (browser.desiredCapabilities.browserName === 'iPhone') {
      this.skip()
    } else {
      var player = $('.carodaVideo')
      var loadingSpinner = $('.carodaVideo .video-js .vjs-loading-spinner')
      browser.waitUntil(function () {
        return !loadingSpinner.isVisible()
      }, 3000, 'expected loading spinner to switch to hide when the video is loaded and starts playing')
      assert.equal(player.getAttribute('class').indexOf('vjs-waiting'), -1)
      browser.pause(3000)
    }
  })

  it('should play video continuously at constant rate', function () {
    if (browser.desiredCapabilities.browserName === 'iPhone') {
      this.skip()
    } else {
      this.timeout(10000)
      var checkInterval = 5000
      var numberOfChecks = 1
      var precision = 1000
      var progressBar = $('.carodaVideo .video-js .vjs-play-progress.vjs-slider-bar')
      var startTimeString = progressBar.getAttribute('data-current-time')
      var startTime = 60000 * startTimeString.split(':')[0] + 1000 * startTimeString.split(':')[1]
      for (var i = 0; i < numberOfChecks; i++) {
        browser.pause(checkInterval)
        var currentTimeString = progressBar.getAttribute('data-current-time')
        var currentTime = 60000 * currentTimeString.split(':')[0] + 1000 * currentTimeString.split(':')[1]
        var expectedTime = startTime + (i + 1) * checkInterval
        assert((currentTime - precision <= expectedTime) && (currentTime + precision >= expectedTime))
      }
    }
  })

  it('should stop playing on pause button click', function () {
    if (browser.desiredCapabilities.browserName === 'iPhone') {
      this.skip()
    } else {
      this.timeout(10000)
      const pauseButton = $('.carodaVideo .vjs-play-control')
      const progressBar = $('.carodaVideo .vjs-play-progress.vjs-slider-bar')
      pauseButton.click()
      const startTimeString = progressBar.getAttribute('data-current-time')
      browser.pause(3000)
      const endTimeString = progressBar.getAttribute('data-current-time')
      assert(startTimeString === endTimeString)
    }
  })
})
